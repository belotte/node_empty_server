const router =	require ('express').Router ()
const liana =	require ('forest-express-sequelize')

if (process.env.NODE_ENV !== 'production') {
	liana.ensureAuthenticated = (req, res, next) => {
		return next ()
	}
}

router.route ('/:action')
.post (liana.ensureAuthenticated, wrap (async (req, res, next) => {
	let action_name = req.params.action.replace (/-/g, '_')
	let model_name = req.body.data.attributes.collection_name.toLowerCase ()
	let action = null
	let errors = []
	let results = []
	let need_html_view = false

	if ((action = models[model_name][action_name]) && typeof action === 'function') {
		try {
			;({ need_html_view, errors = [], ...results } = await action ({ ...req.body.data.attributes.values }, { ids: req.body.data.attributes.ids, user: req.forest_user, from_forest: true }) || { })
			if (results.results) {
				results = results.results
			}
			if (!Array.isArray (results)) {
				results = [{ id: '_', result: results }]
			}
		} catch (error) {
			errors.push ({
				error: error.is_empty && '❌' || error
			})
		}
	}
	/*
	 * prototype action
	 */
	else if ((models[model_name].prototype.at_path (action_name))
		  || (models[model_name].prototype.at_path (action_name.replace (/_/, '.')) && (action_name = action_name.replace (/_/, '.')))
		  || (models[model_name].prototype[`got_${action_name}`] && (action_name = `got_${action_name}`))) {
		let instances = await models[model_name].findAll ({
			where: {
				[models[model_name].primaryKeyAttributes]: {
					[Op.in]: req.body.data.attributes.ids
				}
			}
		})
		for (let instance of instances) {
			action = instance
			for (let part of action_name.split ('.')) {
				action = action[part]
			}
			try {
				;({ need_html_view, error, ...result} = await action.bind (instance) ({ ...req.body.data.attributes.values }, { user: req.forest_user, from_forest: true }) || { })
				if (error) {
					errors.push ({
						id: instance.id || instance.sku || instance.offer_id,
						error: error.is_empty && '❌' || error
					})
				}
				else {
					results.push ({
						id: instance.id || instance.sku || instance.offer_id,
						result: result.is_empty && '✅' || result
					})
				}
			} catch (error) {
				errors.push ({
					id: instance.id || instance.sku || instance.offer_id,
					error: error.is_empty && '❌' || error
				})
			}
		}
	}
	/*
	 * action not found
	 */
	else {
		return res.status (400).send ({
			error: `Action ${model_name}.${action_name} does not exist. Contact @frank.`
		})
	}

	if (errors.length) {
		console.error ([{
			model: model_name,
			action: action_name,
			owner: req.forest_user
		}, ... errors])
		errors.map ((error, index) => errors[index].error && delete errors[index].error.logged)
	}

	if (!need_html_view && errors.length + results.length === 1) {
		return res.status (results[0] && 200 || 400).send ({
			success: JSON.stringify (results[0] && results[0].result),
			error: JSON.stringify (errors[0] && errors[0].error)
		})
	}
	return res.status (200).send ({
		html: `<h1 style = 'color: green; text-decoration: underline;'>SUCCESS (${results.length})</h1>
${results.map (result => `<h3 style = ''>${result.custom_model_name || model_name} #<b>${result.id}</b></h3><pre><code>${JSON.stringify (result.result || '✅', null, 2).replace (/^"/, '').replace (/"$/, '')}</code></pre>`).join ('') || '_'}
<h1 style = 'color: red; text-decoration: underline'>ERROR (${errors.length})</h1>
${errors.map (error => `<h3 style = ''>${error.custom_model_name || model_name} #<b>${error.id}</b></h3><pre><code>${JSON.stringify (error.error, null, 2).replace (/^"/, '').replace (/"$/, '')}</code></pre>`).join ('') || '_'}
<p style = 'display: inline-block; margin-top: 30px; width: 100%; text-align: right;'>Un problème, une question ? Better call <strike>Saul</strike> <a href = 'slack://channel?id=U4SFZBXU2&team=T4RRGJQ1X'>@frank</a>.</p>`
	})
}))

module.exports = router
