/**
 * @namespace Routes.Index
 */

const router =	require ('express').Router ()

router.route ('/')
.get (wrap (async (req, res) => {
	return res.end ('ok')
}))

module.exports = router
