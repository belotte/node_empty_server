module.exports = {
	authSecret: {
		production: '',
		test: ''
	}[process.env.NODE_ENV] || '',
	envSecret: {
		production: '',
		test: ''
	}[process.env.NODE_ENV] || '',
	actions_modifier: action => {
		if (action.fields && action.fields.length) {
			action.name = `${action.name} *`

			for (let index in action.fields) {
				action.fields[index].description = (action.fields[index].description || '').replace (/<[^<]*>/g, '')
			}
		}
		return action
	}
}
