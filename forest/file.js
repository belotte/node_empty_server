const Liana =	require ('forest-express-sequelize')
const { actions_modifier } = require (`${process.env.PWD}/config/forest`)

Liana.collection ('file', {
	actions: [{
		name: 'add_remark',
		fields: [{
			field: 'remark',
			type: 'String'
		}]
	}, {
		name: 'download',
		fields: []
	}, {
		name: 'upload',
		fields: [{
			field: ' ',
			type: 'description',
			description: `
<h2>Description</h2>
Upload un fichier sur le serveur.
Ce fichier sera accessible par n'importe qui possédant le lien.
Il est possible de désactiver ou de réactiver le lien a tout moment avec les actions <code>disable</code> et <code>enable</code>.`
		}, {
			field: 'file_',
			type: 'File',
			isRequired: true,
			description: `\
<p>Fichier à uploader. Il sera mis sur le serveur et disponible au téléchargement via un lien.</p>`
		}, {
			field: 'owner',
			type: 'String',
			description: `\
<p>Propriétaire. Utilisé uniquement à titre indicatif.</p>`
		}, {
			field: 'filename',
			type: 'String',
			isRequired: true,
			description: `\
<p>Nom du fichier. Il faut spécifier l'extension.</p><p>Si un fichier existe deja avec ce nom et le même type, il sera écrasé.</p>`
		}, {
			field: 'type',
			type: 'Enum',
			enums: models.file.rawAttributes.type.values,
			defaultValue: 'file',
			isRequired: true,
			description: `\
<p>Type du fichier. Utilisé uniquement à titre indicatif.</p>`
		}, {
			field: 'enabled',
			type: 'Boolean',
			defaultValue: true,
			description: `\
Si non coché en <p style = 'display: inline-block; color: green;'><b>vert</b></p>, le lien <u>ne fonctionnera pas</u>.
Il est possible de modifier ce paramètre ultérieurement.`
		}],
		type: 'global'
	}, {
		name: 'enable',
		fields: []
	}, {
		name: 'disable',
		fields: []
	}].map (actions_modifier),

	fields: [{
		field: 'link',
		get: file => `${hosts.local}/api/v2/files/${file.token}`
	}]
})
