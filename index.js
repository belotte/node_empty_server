/*---------------------------REQUIREMENTS-------------------------------------*/
Object.keys (require ('./.env')).map (key => process.env[key.toUpperCase ()] = require ('./.env')[key])
/*	Charge les ptototypes et variables globales. */
require (`${process.env.PWD}/api/services/globals`)

const express = require ('express')
const app = express ()
const router = express.Router ()
const server = require ('http').createServer (app)
const forest_config = require (`${process.env.PWD}/config/forest`)
const { sequelize } = require (`${process.env.PWD}/api/services/sequelize`)
const mdw = require (`${process.env.PWD}/api/services/middleware`)
const Logger = require (`${process.env.PWD}/api/services/logger`)
/*	Initialize un Logger, qui va ecrire dans le fichier ./logs/log.txt */
const loggers = {
	request: Logger ({
		path: 'requests.json',
		is_request: true
	}),
	forest: Logger ({
		path: 'forest.json',
		is_request: true
	}),
	crons: Logger ({
		path: 'crons.json',
		is_request: true
	})
}
/*----------------------------------------------------------------------------*/

/*---------------------------MIDDLEWARES--------------------------------------*/
app.set ('view engine', 'pug')
app.use (require ('cors') ({
	origin: [
		'http://app.forestadmin.com',
		'https://app.forestadmin.com',
		'http://future.forestadmin.com',
		'https://future.forestadmin.com',
		'http://localhost:3000'
	],
	credentials: true
}))
app.use (require ('helmet') ())
app.use (express.urlencoded ({ limit: '50mb' }))
app.use (express.json ({ limit: '50mb' }))
if (forest_config.envSecret) {
	app.use (require ('forest-express-sequelize').init ({
		modelsDir: `${process.env.PWD}/api/models`,
		...forest_config,
		sequelize: sequelize
	}))
}
app.use ('/js',				express.static ('assets/js'))
app.use ('/css',				express.static ('assets/css'))
app.use ('/icons',			express.static ('assets/icons'))
app.use ('/images',			express.static ('assets/images'))
app.use ('/ressources',		express.static ('assets/ressources'))
app.use ('/.well-known',	express.static( 'assets/.well-known', { dotfiles: 'allow' } ))

app.use ('/',						require (`${process.env.PWD}/routes/index.js`))
app.use ('/ping',					require (`${process.env.PWD}/routes/ping.js`))
app.use ('/tools',				require (`${process.env.PWD}/routes/tools.js`))
app.use ('/api/v1/crons',		require (`${process.env.PWD}/routes/cron.js`))
app.use ('/api/v1/files',		require (`${process.env.PWD}/routes/file.js`))
app.use ('/forest/actions',	require (`${process.env.PWD}/routes/forest/actions.js`))

app.use (async (error, req, res, next) => {
	error = {
		timestamp: now (),
		error: error.to_string && error.to_string () || error
	}

	let error_instance = req.query.ERROR_ID && await models.error.findOne ({
		where: {
			id: req.query.ERROR_ID
		}
	}) || null

	if (error_instance) {
		await error_instance.update ({
			errors: [error, ...error_instance.errors]
		})
	}
	else {
		await models.error.create ({
			route: req.originalUrl,
			method: req.method,
			body: req.body,
			parameters: {
				query: req.query,
				params: req.params
			},
			headers: req.headers,
			errors: [error]
		})
	}
	try {
		if (error && !error.logged && !error.error.logged) {
			console.error (error)
		}
		delete error.logged
		delete error.error.logged
	} catch (error) { }
	return res.status (500).json (error && !error.is_empty && error || {
		error: 'An error occured. Please contact belotte1355@gmail.com.'
	})
})

app.use ('*', (req, res) => {
	return res.status (404).end ('404')
})

server.listen (process.env.PORT, async () => {
	console.info (`Server running on port ${process.env.PORT.toString ().cyan.bold}.`)
	console.info (`Environment: ${process.env.NODE_ENV.toUpperCase ()[{
		test: 'blue',
		development: 'green',
		production: 'red'
	}[process.env.NODE_ENV] || 'blue'].bold}`)

	await wait (1.5)
	process.emit ('server_started')
})

module.exports = app
