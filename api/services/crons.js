/**
 * Ce fichier liste toutes les fonctions qui sont declenchees par un cron (`$ crontab -h`).
 *
 * Elles sont accessibles via **GET** `<host>/api/v2/crons/run/:function_name`.
 *
 * @module cron
 */

const sample = function () {

}

module.exports = {
	sample
}
